%% System of Differential Equations
% This function solves a system of ordinary differential equations which
% determine the concentrations of two subtances, A and B at various times.
%
% This code requires a time value, in hours, Concentration values, in mg/L,
% and 4 k constants, with units of h^-1, h^-1, mg L^-1 h^-1, and h^-1
% respectively. 
%
% This code outputs a column vector containing the rate of change of
% concentration for A and B respectively. 
%
% The expected syntax of this code is system_of_ODEs(t, [Ca Cb], k1, k2,
% k3, k4), though inputs of only time and concentration, or no inputs at
% all are also acceptable. 
%
% This code was written and produced by Phillip Pergande on September
% 10th, 2019

function output = system_of_ODEs(varargin)

% Below are the two differential equations for which solutions are sought
%%
% 
% $$\frac{dC_a}{dt} =-k_1C_a-k_2C_a$$
%
% $$\frac{dC_b}{dt} = k_1C_a-k_3-k_4C_b$$

%% Create a set of default values which can be changed here, and be called when needed.
% Default Values
t_default = 0; %unit h
Ca_default = 6.25; %unit mgL^-1
Cb_default = 0; %unit mgL^-1
k1_default = 0.15; %unit h^-1
k2_default = 0.6; %unit h^-1
k3_default = 0.1; %unit mgL^-1h^-1
k4_default = 0.2; %unit h^-1

%% Check for number of inputs

%see how many inputs we have 
if nargin == 6 %if we have 6 inputs, set each variable equal to what the user defined
    t = varargin{1}; 
    Cdummy = varargin{2}; %create a dummy variable to hold vector
    Ca = Cdummy(1); %call Ca value from row vector
    Cb = Cdummy(2); %call Cb value from row vector
    k1 = varargin{3}; 
    k2 = varargin{4};
    k3 = varargin{5};
    k4 = varargin{6};
    disp('6 inputs given, user values used for all calculations')
end

if nargin == 2 %if we have 2 inputs, define default values for ks
    t = varargin{1};
    Cdummy = varargin{2};
    Ca = Cdummy(1);
    Cb = Cdummy(2);
    k1 = k1_default; %unit h^-1
    k2 = k2_default; %unit h^-1
    k3 = k3_default; %unit mg L^-1 h^-1
    k4 = k4_default; %unit h^-1
    disp('2 inputs given. Default values used for k-values')
    disp('The expected syntax is system_of_ODEs(t, [Ca Cb], k1, k2, k3, k4)')
end

if nargin == 0 %if no input whatsoever is specified, use default values
    t = t_default; %unit h
    Ca = Ca_default; %unit mgL^-1
    Cb = Cb_default; %unit mgL^-1
    k1 = k1_default; %unit h^-1
    k2 = k2_default; %unit h^-1
    k3 = k3_default; %unit mgL^-1 h^-1
    k4 = k4_default; %unit h^-1
    disp('No inputs given, default values used')
    disp('The expected syntax is system_of_ODEs(t, [Ca Cb], k1, k2, k3, k4)')
end


%% Run the differential equations
%run the differential equations with the specified values 

output(1) = -k1*Ca-k2*Ca;
output(2) = k1*Ca-k3-k4*Cb;
%% Supply the answer to the user in the form of a column vector
output = output';
end
