%% solve_ODEs_CA3
%%This function will output the results of a given set of differential
%%equations designed to analyze the conditions of a PFR at a given set of
%%conditions 

% This code was produced by Phillip Pergande on September 24, 2019 for Dr.
% Ford Verspyt's Advanced Numerical Science course at Oklahoma State
% University. 
% 
% This function takes no input from the user, instead relying on a set of
% pre-defined conditions available before the ode function is called.
% Modify these if necessary for your purposes. 

% This code outputs plots which analyze the change in the flowrates of
% species A, B, and C, as well as the change in temperature with respect to
% the volume of the system. 
%% Define the function and some initial values
function solve_ODEs_CA3

T0 = 423; %Kelvin
Cpa = 90; %J/molC
Cpb = 90; %J/molC
Cpc = 180; %J/molC
Ua = 4000; %J/m3 s C
Ta = 373; %Kelvin 
E1R = 4000; %Kelvin
E2R = 9000; %Kelvin
Ct0 = 0.1;
HRXN1 = -20000; %J/mol reaction A
HRXN2 = -60000; %J/mol reaction B

%% Call the ode45 solver to analyze the system of differential equations. 
% Specify default values for Fa, Fb, Fc, and T, and set the volume to be
% analyzed over. In this case 1L is used. 


[V, F_and_T] = ode45(@ODEs_CA3, [0:0.01:1], [100 0 0 423]);

%% embed the differential equation function within the larger function
    function dxdV = ODEs_CA3(V, F_and_T) 
        Fa = F_and_T(1); %collect F values from vector
        Fb = F_and_T(2);
        Fc = F_and_T(3);
        Ft = Fa+Fb+Fc;
        T = F_and_T(4);
        Ca = Ct0*(Fa/Ft)*(T0/T); %calculate concentrations
        Cb = Ct0*(Fb/Ft)*(T0/T);
        Cc = Ct0*(Fc/Ft)*(T0/T);
        k1a = 10*exp(E1R*((1/300)-(1/T))); %calculate kinetic properties
        k2a = 0.09*exp(E2R*((1/300)-(1/T)));
        r1a = -k1a*Ca; %calculate rates
        r2a = -k2a*Ca*Ca;
        ra = r1a+r2a; %combine rates into rates respective to species 
        rb = k1a*Ca;
        rc = 0.5*k2a*Ca^2;
        TwrtV = (Ua*(Ta-T)+(-r1a)*(-HRXN1)+(-r2a)*(-HRXN2))/((Fa*Cpa)+(Fb*Cpb)+(Fc*Cpc)); %generate dT/dV equation
        dxdV = [ra; rb; rc; TwrtV]; %put all results into a vector for output
    end

%% plot the results 
figure(1)
plot(V, F_and_T(:,4)), legend('Temperature'), title('Temperature(K) vs. Volume(L)')
xlabel('Volume in L'), ylabel('Temperature in Kelvin')
figure(2)
plot(V, F_and_T(:,1), V, F_and_T(:,2), V, F_and_T(:,3)), legend('Fa', 'Fb', 'Fc')
xlabel('Volume in L'), ylabel('Molar Flowrate by species (mol/s)'), title('Flow Rate vs. Volume')

end

